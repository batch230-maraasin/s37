const mongoose = require("mongoose");
const Course = require("../models/course.js");

/*
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((newCourse, error)=>
	{
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
		
	})
}
*/


// GET all course
module.exports.getAllCourse = () => {
	return Course.find({}).then(result =>{
		return result;
	})
}

// GET all active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}

// GET specific course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result =>{
		return result;
	})
}

// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

//--------------------------- Activity //

// Function for adding a course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course.
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin
		// request from POSTMAN Body = reqBody


module.exports.addCourse = (reqBody, addedCourse) => {
	if(addedCourse.isAdmin == true){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			slots: reqBody.slots
		})						//variable
	return newCourse.save().then((newCourse, error)=>{
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

//-------------------------------------------//s40

module.exports.archiveCourse = (courseId, archiveData) => {
	if(archiveData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				
				isActive: archiveData.course.isActive, 
				
			}
		).then((result, error)=>{
			if(error){
				return false;
			} else{
			return true
			}
		})

	
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}