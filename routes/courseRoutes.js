const express = require ("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

/*
router.post("/create", (request, response) =>{
	courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))
})
*/

// GET all Courses/ no function Name beacuse same on endpoint
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
})

// GET all ACTIVE Courses
router.get("/active", (request, response) => {
	courseControllers.getActiveCourses().then(resultFromController => response.send(resultFromController))
});

// GET Specific course
router.get("/:courseId", (request, response) =>{
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController));
})

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

//-----------------------------Activity //
//Creating a course
// Activity
// 1. Update the "course" route to implement "user authentication for the admin" when creating a course.

router.post("/create", auth.verify, (request, response) =>{
	const addedCourse = {
		course: request.body,
			//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	courseControllers.addCourse(request.body, addedCourse).then(resultFromController => 
		response.send(resultFromController)
	)
})

//----------------------------------------//s40

router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const archiveData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.archiveCourse(request.params.courseId, archiveData).then(resultFromController => {
		response.send(resultFromController)
	})
})

module.exports = router;